package ar.edu.undec.adapter.data.persona.crud;

import ar.edu.undec.adapter.data.persona.model.PersonaEntity;
import org.springframework.data.repository.CrudRepository;

public interface ActualizarPersonaCRUD extends CrudRepository<PersonaEntity,Integer> {
}
