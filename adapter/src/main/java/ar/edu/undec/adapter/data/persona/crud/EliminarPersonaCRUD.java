package ar.edu.undec.adapter.data.persona.crud;

import ar.edu.undec.adapter.data.persona.model.PersonaEntity;
import org.springframework.data.repository.CrudRepository;

public interface EliminarPersonaCRUD extends CrudRepository<PersonaEntity,Integer>{
    void deleteById(Integer id);
}