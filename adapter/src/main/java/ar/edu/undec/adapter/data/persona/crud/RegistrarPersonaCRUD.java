package ar.edu.undec.adapter.data.persona.crud;

import ar.edu.undec.adapter.data.persona.model.PersonaEntity;
import org.springframework.data.repository.CrudRepository;

public interface RegistrarPersonaCRUD extends CrudRepository<PersonaEntity, Integer> {
    boolean existsByNombreAndApellido(String nombre,String apellido);
    PersonaEntity save(PersonaEntity personaEntity);
}
