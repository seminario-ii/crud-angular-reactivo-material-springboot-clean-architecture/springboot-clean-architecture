package ar.edu.undec.adapter.data.persona.mapper;

import ar.edu.undec.adapter.data.persona.model.PersonaEntity;
import persona.exception.PersonaException;
import persona.model.Persona;

public class PersonaDataMapper{

    public static Persona dataCoreMapper(PersonaEntity personaEntity){
        try{
            return Persona.getInstance(personaEntity.getId(),personaEntity.getNombre(),personaEntity.getApellido(),personaEntity.getDni(),personaEntity.getFechaNacimiento());
        }catch (RuntimeException e){
            throw new PersonaException("Error al mapear de entity a core");
        }
    }

    public static PersonaEntity coreDataMapper(Persona persona){
        try{
            return new PersonaEntity(persona.getId(),persona.getNombre(),persona.getApellido(),persona.getDni(),persona.getFechaNacimiento());
        }catch (RuntimeException e){
            throw new PersonaException("Error al mapear de core a entity");
        }
    }
}
