package ar.edu.undec.adapter.data.persona.repoImplementation;

import ar.edu.undec.adapter.data.persona.crud.RegistrarPersonaCRUD;
import ar.edu.undec.adapter.data.persona.exception.DataBaseException;
import ar.edu.undec.adapter.data.persona.mapper.PersonaDataMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import persona.model.Persona;
import persona.output.RegistrarPersonaRepository;

@Service
public class RegistrarPersonaRepoImplementation implements RegistrarPersonaRepository {

    RegistrarPersonaCRUD registrarPersonaCRUD;

    @Autowired
    public RegistrarPersonaRepoImplementation(RegistrarPersonaCRUD registrarPersonaCRUD) {
        this.registrarPersonaCRUD = registrarPersonaCRUD;
    }

    @Override
    public boolean existePersona(String nombre, String apellido) {
        return registrarPersonaCRUD.existsByNombreAndApellido(nombre,apellido);
    }

    @Override
    public Integer registrarPersona(Persona persona) {
        try{
            return registrarPersonaCRUD.save(PersonaDataMapper.coreDataMapper(persona)).getId();
        }catch (RuntimeException e){
            throw new DataBaseException("Error al guardar en la base de datos");
        }
    }
}
