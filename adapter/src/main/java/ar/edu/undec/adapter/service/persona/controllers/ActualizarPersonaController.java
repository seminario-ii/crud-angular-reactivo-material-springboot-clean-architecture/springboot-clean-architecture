package ar.edu.undec.adapter.service.persona.controllers;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import persona.input.ActualizarPersonaInput;
import persona.usecase.actualizarPersonaUseCase.ActualizarPersonaRequestModel;

@RestController
@RequestMapping("personas")
@CrossOrigin("*")
public class ActualizarPersonaController{
    ActualizarPersonaInput actualizarPersonaInput;

    public ActualizarPersonaController(ActualizarPersonaInput actualizarPersonaInput) {
        this.actualizarPersonaInput = actualizarPersonaInput;
    }

    @PutMapping
    public ResponseEntity<?> actualizarPersona(@RequestBody ActualizarPersonaRequestModel actualizarPersonaRequestModel){
        try{
            return ResponseEntity.ok(actualizarPersonaInput.actualizarPersona(actualizarPersonaRequestModel));
        }catch (RuntimeException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
}
