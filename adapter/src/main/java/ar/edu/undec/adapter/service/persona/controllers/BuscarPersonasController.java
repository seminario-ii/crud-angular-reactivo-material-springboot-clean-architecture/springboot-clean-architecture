package ar.edu.undec.adapter.service.persona.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import persona.input.BuscarPersonasInput;

@RestController
@RequestMapping("personas")
@CrossOrigin(origins = "*")
public class BuscarPersonasController {
    BuscarPersonasInput buscarPersonasInput;

    public BuscarPersonasController(BuscarPersonasInput buscarPersonasInput) {
        this.buscarPersonasInput = buscarPersonasInput;
    }

    @GetMapping
    ResponseEntity<?> buscarPersonas(){
        try{
            return ResponseEntity.ok(buscarPersonasInput.buscarPersonas());
        }catch (RuntimeException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }


}
