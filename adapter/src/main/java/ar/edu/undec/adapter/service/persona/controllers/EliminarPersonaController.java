package ar.edu.undec.adapter.service.persona.controllers;

import org.apache.coyote.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import persona.input.EliminarPersonaInput;

@RestController
@RequestMapping("personas")
@CrossOrigin("*")
public class EliminarPersonaController{

    EliminarPersonaInput eliminarPersonaInput;

    public EliminarPersonaController(EliminarPersonaInput eliminarPersonaInput) {
        this.eliminarPersonaInput = eliminarPersonaInput;
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> eliminarPersona(@PathVariable Integer id){
        try{
            return ResponseEntity.ok(eliminarPersonaInput.eliminarPersona(id));
        }catch (RuntimeException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }

    }
}
