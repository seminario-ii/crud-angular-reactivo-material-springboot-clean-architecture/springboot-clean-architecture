package ar.edu.undec.adapter.service.persona.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import persona.exception.PersonaException;
import persona.input.RegistrarPersonaInput;
import persona.usecase.registrarPersonaUseCase.RegistrarPersonaRequestModel;

@RestController
@RequestMapping("personas")
@CrossOrigin(origins = "*")
public class RegistrarPersonaController{

    RegistrarPersonaInput registrarPersonaInput;

    public RegistrarPersonaController(RegistrarPersonaInput registrarPersonaInput) {
        this.registrarPersonaInput = registrarPersonaInput;
    }

    @PostMapping
    public ResponseEntity<?> registrarPersona(@RequestBody RegistrarPersonaRequestModel registrarPersonaRequestModel){
        try{
            return ResponseEntity.ok().body(registrarPersonaInput.registrarPersona(registrarPersonaRequestModel));
        }catch(RuntimeException personaException) {
            return ResponseEntity.badRequest().body(personaException.getMessage());
        }
    }
}
