package persona.input;

import persona.model.Persona;

import java.util.Collection;

public interface BuscarPersonasInput {
    Collection<Persona> buscarPersonas();
}
