package persona.input;

public interface EliminarPersonaInput{
    boolean eliminarPersona(Integer id);
}
