package persona.input;

import persona.usecase.registrarPersonaUseCase.RegistrarPersonaRequestModel;

public interface RegistrarPersonaInput{
    Integer registrarPersona(RegistrarPersonaRequestModel registrarPersonaRequestModel);
}
