package persona.output;

import persona.model.Persona;

public interface ActualizarPersonaRepository{
    boolean actualizarPersona(Persona persona);
}
