package persona.output;

import persona.model.Persona;

import java.util.Optional;

public interface BuscarPersonaRepository {
    Optional<Persona> buscarPersona(Integer id);
}
