package persona.output;

public interface EliminarPersonaRepository {
    boolean eliminarPersona(Integer id);
}
