package persona.output;

import persona.model.Persona;

public interface RegistrarPersonaRepository {

    boolean existePersona(String nombre,String apellido);
    Integer registrarPersona(Persona persona);
}
