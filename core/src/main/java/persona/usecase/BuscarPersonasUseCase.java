package persona.usecase;

import persona.input.BuscarPersonasInput;
import persona.model.Persona;
import persona.output.BuscarPersonasRepository;

import java.util.Collection;

public class BuscarPersonasUseCase implements BuscarPersonasInput {

    BuscarPersonasRepository buscarPersonasRepository;

    public BuscarPersonasUseCase(BuscarPersonasRepository buscarPersonasRepository) {
        this.buscarPersonasRepository = buscarPersonasRepository;
    }

    @Override
    public Collection<Persona> buscarPersonas() {
        return buscarPersonasRepository.buscarPersonas();
    }
}
