package persona.usecase;

import persona.exception.PersonaException;
import persona.input.EliminarPersonaInput;
import persona.output.BuscarPersonaRepository;
import persona.output.EliminarPersonaRepository;

public class EliminarPersonaUseCase implements EliminarPersonaInput {

    EliminarPersonaRepository eliminarPersonaRepository;
    BuscarPersonaRepository buscarPersonaRepository;

    public EliminarPersonaUseCase(EliminarPersonaRepository eliminarPersonaRepository,BuscarPersonaRepository buscarPersonaRepository) {
        this.eliminarPersonaRepository = eliminarPersonaRepository;
        this.buscarPersonaRepository = buscarPersonaRepository;
    }
    @Override
    public boolean eliminarPersona(Integer id){
        if(buscarPersonaRepository.buscarPersona(id).isEmpty()){
            throw new PersonaException("La persona no existe");
        }
        return eliminarPersonaRepository.eliminarPersona(id);
    }
}
