package persona.usecase.actualizarPersonaUseCase;

import java.time.LocalDate;

public class ActualizarPersonaRequestModel {
    private Integer id;
    private String nombre;
    private String apellido;
    private String dni;
    private LocalDate fechaNacimiento;

    public ActualizarPersonaRequestModel(){}
    public ActualizarPersonaRequestModel(Integer id,String nombre, String apellido, String dni, LocalDate fechaNacimiento) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.dni = dni;
        this.fechaNacimiento = fechaNacimiento;
    }


    public Integer getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getDni() {
        return dni;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }
}
