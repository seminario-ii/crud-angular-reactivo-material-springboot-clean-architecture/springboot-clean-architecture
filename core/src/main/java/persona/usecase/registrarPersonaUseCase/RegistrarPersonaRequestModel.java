package persona.usecase.registrarPersonaUseCase;

import java.time.LocalDate;

public class RegistrarPersonaRequestModel {
    private String nombre;
    private String apellido;
    private String dni;
    private LocalDate fechaNacimiento;

    public RegistrarPersonaRequestModel(){}
    public RegistrarPersonaRequestModel(String nombre, String apellido, String dni, LocalDate fechaNacimiento) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.dni = dni;
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getDni() {
        return dni;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }
}
